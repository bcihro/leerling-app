<?php
include_once('config.php');

$locations5gz = array(
	'f8:4f:57:3b:2d:cf' => '4.014',
    'f8:4f:57:78:bf:8f' => '4.016',
    'f8:4f:57:07:b8:ef' => '4.016',
    'f8:4f:57:30:6f:9f' => '4.017',
    'f8:4f:57:3b:32:5f' => '4.022',
    'f8:4f:57:3a:44:2f' => '4.023'
);

$locations24gz = array(
	'f8:4f:57:3b:2d:c0' => '4.014',
	'f8:4f:57:78:bf:80' => '4.016',
	'f8:4f:57:13:c3:00' => '4.016',
	'f8:4f:57:3b:32:50' => '4.022',
	'f8:4f:57:3a:44:20' => '4.023'
);

if(isset($_POST['wifi_data']) && isset($_POST['docent_code'])){


    $wifi_data = $_POST['wifi_data'];
    
    $jsonContent = json_decode($wifi_data,true);
    
    $docent_code = $_POST['docent_code'];
    
    $docent_name = $_POST['docent_naam'];   
    
    foreach($jsonContent as $content) {
    
	    $wifi_info[] = json_decode($content,true);
	    
    }
    
    $docent_data = sortData($wifi_info, $docent_code);

    $levels = current($docent_data);
    $current_BSSID = current($levels);
    
	if(array_key_exists($current_BSSID[0], $locations5gz)){
		$current_location = $locations5gz[$current_BSSID[0]];
	}
	elseif(array_key_exists($current_BSSID[0], $locations24gz)) {
		$current_location = $locations24gz[$current_BSSID[0]];
	}
	else {
		$current_location = 'Niet in de hogeschool';
	}

	$current_date = Date('d-m-Y');   
	$current_time = Date('H:i');


    $delete_row = "DELETE FROM docent_data WHERE docent_code='". $docent_code . "'";
    mysqli_query($connection, $delete_row);

    $query = 'INSERT INTO docent_data (docent_code, locatie, docent_name, datum, tijd)
                VALUES ("' . $docent_code . '",
                        "' . $current_location . '",
                        "' . $docent_name . '",
                        "' . $current_date . '",
                        "' . $current_time . '")';

    mysqli_query($connection, $query);

    echo $current_location;

}else{

    echo "FAIL";
    
}

function sortData($array, $docentCode){
    $tmp = array();
    $result = array();

    foreach($array as $data){
        $tmp[] = &$data['level'];
    }
    array_multisort($tmp, SORT_DESC, $array);
    foreach($array as $data){
        $level = $data['level'];
        $BSSID = $data['BSSID'];
        $docent_code = $docentCode;
        if(array_key_exists( $level, $result)){
            $result[$docent_code][$level][] = $BSSID;
        }else{
            $result[$docent_code][$level] = array();
            $result[$docent_code][$level][] = $BSSID;
        }
    }
    return $result;
}

?>
