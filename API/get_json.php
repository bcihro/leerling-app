<?php
include_once('config.php');
/**
 * Created by PhpStorm.
 * User: Michael Parry
 * Date: 17-4-14
 * Time: 12:23
 */
header('Content-type: application/json');

$query = "SELECT * from docent_data ORDER BY docent_name ASC";
$resultProject = mysqli_query($connection, $query);

$json = array('locations' => array());

while($row = mysqli_fetch_assoc($resultProject)){

	$timestamp = strtotime($row['datum'] . " " . $row['tijd']);
	$timestamp2 = $timestamp + 7200;
	$currentTimestamp = (time() + 7200);
	$diff = $currentTimestamp - $timestamp2;
	$minutes = $diff / 60;
	
	$row['timediff'] = round($minutes);
    $json['locations'][] = $row;
}

sleep(2);
echo json_encode($json);
