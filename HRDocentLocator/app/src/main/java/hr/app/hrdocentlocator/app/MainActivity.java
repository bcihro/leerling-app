package hr.app.hrdocentlocator.app;

import android.app.ActionBar;
import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.*;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class MainActivity extends Activity {

    private ScheduledExecutorService locationService;
    private NotificationManager mNotificationManager;
    private SharedPreferences settings;
    private TextView statusText;
    private TextView locationText;
    private ProgressBar loader;
    private Menu menu;
    private WifiManager wifiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        settings = PreferenceManager.getDefaultSharedPreferences(this);
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);

        statusText = (TextView) findViewById(R.id.statusText);
        locationText = (TextView) findViewById(R.id.locationText);
        loader = (ProgressBar) findViewById(R.id.progressBar);

        Log.d("wifistate", Integer.toString(wifiManager.getWifiState()) );

        wifiManager.startScan();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);

        this.menu = menu;

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
                return true;
            case R.id.location_service:
                serviceManager(item);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void openSettings() {


        Intent intent = new Intent(this, SettingsActivity.class);

        startActivity(intent);

    }

    public void serviceManager(MenuItem item) {

        if(item.getTitle().toString() == getString(R.string.action_start)) {
            item.setTitle(R.string.action_stop);
            Toast.makeText(this, "Location service started", Toast.LENGTH_SHORT).show();
            startLocationService();

        }
        else if(item.getTitle().toString() == getString(R.string.action_stop)) {
            item.setTitle(R.string.action_start);
            Toast.makeText(this, "Location service stopped", Toast.LENGTH_SHORT).show();
            stopLocationService();

        }
    }

    public void startLocationService() {

        String refreshRate = settings.getString("sync_frequency", "10");

        int refreshRateINT = Integer.parseInt(refreshRate);

        locationService = Executors.newScheduledThreadPool(5);

        locationService.scheduleAtFixedRate(new Runnable() {
            public void run() {

                runOnUiThread(findAndSendLocation);

            }
        }, 0, refreshRateINT, TimeUnit.SECONDS);

        mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon(R.drawable.ic_launcher);
        mBuilder.setContentTitle("Docenten Finder lookout");
        mBuilder.setContentText("Op dit moment wordt uw locatie doorgeven");
        mBuilder.setOngoing(true);

        mNotificationManager.notify(100, mBuilder.build());


    }

    public void stopLocationService() {

        statusText.setText("Inactief");

        loader.setVisibility(View.INVISIBLE);

        locationService.shutdown();

        mNotificationManager.cancel(100);

    }

    public Runnable findAndSendLocation = new Runnable() {

        public void run() {

            wifiManager.startScan();

            statusText.setText("Locatie ophalen en verzenden");
            loader.setVisibility(View.VISIBLE);

            Gson gson = new Gson();

            String docent_code = settings.getString("docent_code", "notGiven");
            String docent_naam = settings.getString("docent_naam", "notGiven");

            AsyncHttpClient client = new AsyncHttpClient();

            List wifiResults = wifiManager.getScanResults();

            ArrayList<String> wifi = new ArrayList<String>();

            for (Object wif : wifiResults) {

                wifi.add(gson.toJson(wif));

            }

            String json = gson.toJson(wifi);

            RequestParams params = new RequestParams();
            params.put("wifi_data", json);
            params.put("docent_code", docent_code);
            params.put("docent_naam", docent_naam);

            //System.out.println(params.toString());

            client.post("http://docentfinder.martijnschipper.com", params, new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(String response) {

                    //System.out.println(response);

                    locationText.setText(response);
                    loader.setVisibility(View.INVISIBLE);
                    statusText.setText("Locatie verzonden");
                    wifiManager.startScan();

                }

                @Override
                public void onFailure(Throwable error) {

                    statusText.setText("Er is geen werkende internetverbinding");

                    serviceManager(menu.findItem(R.id.location_service));

                }

            });

        }

    };

    @Override
    public void onDestroy() {

        super.onDestroy();

        locationService.shutdown();

        mNotificationManager.cancel(100);
    }



}
