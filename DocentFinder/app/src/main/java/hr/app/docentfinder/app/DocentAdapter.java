package hr.app.docentfinder.app;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Michael Parry on 23-4-2014.
 */
public class DocentAdapter extends ArrayAdapter<String> {

    Context context;
    int resID;
    ArrayList<String> items = null;

    public DocentAdapter(Context context, int resID, ArrayList<String> items) {
        super(context, resID, items);
        this.context = context;
        this.resID = resID;
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if(row == null)
        {
            row = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_block, null);
        }

        Docent docentData = Docent.data.get(position);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String date = dateFormat.format(cal.getTime());



        TextView listTitle = (TextView) row.findViewById(R.id.txtTitle);
        listTitle.setText(docentData.docentNaam + " (" + docentData.docentCode + ")");

        if(docentData.datum.equals(date) && !docentData.locatie.equals("Niet in de hogeschool")){
            listTitle.setBackgroundColor(Color.TRANSPARENT);
            listTitle.setTextColor(Color.BLACK);
            int  $parser = Integer.parseInt(docentData.verschil);
            if($parser <= 15){
                listTitle.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.green, 0);
            }else if($parser <= 60 && $parser > 15){
                listTitle.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.orange, 0);
            }else{
                listTitle.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.red,0);
            }

        }else{
            listTitle.setBackgroundColor(Color.LTGRAY);
            listTitle.setTextColor(Color.DKGRAY);
            listTitle.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.red,0);
        }

        return listTitle;
    }


}
