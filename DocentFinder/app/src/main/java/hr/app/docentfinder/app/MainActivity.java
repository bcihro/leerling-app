package hr.app.docentfinder.app;

import java.util.ArrayList;
import java.util.Locale;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v13.app.FragmentPagerAdapter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity implements ActionBar.TabListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    public static final String TAG_DOCENTNAAM      = "docent_name";
    public static final String TAG_DOCENTCODE      = "docent_code";
    public static final String TAG_DATUM           = "datum";
    public static final String TAG_TIJD            = "tijd";
    public static final String TAG_LOCATIE         = "locatie";
    public static final String TAG_ID              = "id";
    public static final String TAG_VERSCHIL        = "timediff";

    private ArrayAdapter<String> adapter;
    LijstFragment l;
    PlattegrondFragment p;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    public static MainActivity context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
        if (id == R.id.action_refresh){
            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://docentfinder.martijnschipper.com/get_json.php", new AsyncHttpResponseHandler() {

                @Override
                public void onSuccess(String response) {
                    Docent.data = new ArrayList<Docent>();
                    JSONObject obj;
                    try {
                        obj = new JSONObject(response);
                        JSONArray locations = obj.getJSONArray("locations");

                        for (int i = 0; i < locations.length(); i++) {
                            JSONObject l = locations.getJSONObject(i);

                            String id = l.getString(MainActivity.TAG_ID);
                            String docentNaam = l.getString(MainActivity.TAG_DOCENTNAAM);
                            String docentCode = l.getString(MainActivity.TAG_DOCENTCODE);
                            String locatie = l.getString(MainActivity.TAG_LOCATIE);
                            String datum = l.getString(MainActivity.TAG_DATUM);
                            String tijd = l.getString(MainActivity.TAG_TIJD);
                            String verschil = l.getString(MainActivity.TAG_VERSCHIL);

                            Docent docent = new Docent(id, docentCode, docentNaam, locatie, datum, tijd, verschil);
                            Docent.data.add(docent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    ArrayList<String> docentData = new ArrayList<String>();
                    for (Docent d : Docent.data) {
                        docentData.add(d.docentNaam + " (" + d.docentCode + ")");
                    }

                    l.Refresh();
                    p.refresh();

                }
            });

        }else if(id == R.id.action_info){
            Intent i = new Intent(this, InfoActivity.class);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            Fragment returnValue;

            switch(position){
                case 0:
                    p = PlattegrondFragment.newInstance((position + 1));
                    returnValue = p;
                    break;
                case 1:
                    l = LijstFragment.newInstance((position + 1));
                    returnValue = l;
                    break;
                default:
                    returnValue = PlaceholderFragment.newInstance(position + 1);
                    break;
            }

            return returnValue;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Locale l = Locale.getDefault();
            switch (position) {
                case 0:
                    return getString(R.string.title_section1).toUpperCase(l);
                case 1:
                    return getString(R.string.title_section2).toUpperCase(l);
                case 2:
                    return getString(R.string.title_section3).toUpperCase(l);
            }
            return null;
        }
    }


}
