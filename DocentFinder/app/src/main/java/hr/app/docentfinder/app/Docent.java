package hr.app.docentfinder.app;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Michael Parry on 17-4-2014.
 */
public class Docent implements Serializable{

    public String id;
    public String docentCode;
    public String docentNaam;
    public String locatie;
    public String datum;
    public String tijd;
    public String verschil;
    public static ArrayList<Docent> data;

    public Docent(String id, String docentCode, String docentNaam, String locatie, String datum, String tijd, String verschil){
        this.id         = id;
        this.docentNaam = docentNaam;
        this.docentCode = docentCode;
        this.locatie    = locatie;
        this.datum      = datum;
        this.tijd       = tijd;
        this.verschil   = verschil;
    }


}
