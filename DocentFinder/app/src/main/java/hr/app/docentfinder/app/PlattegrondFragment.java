package hr.app.docentfinder.app;

/**
 * Created by Michael Parry on 17-4-2014.
 */

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlattegrondFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlattegrondFragment newInstance(int sectionNumber) {
        PlattegrondFragment fragment = new PlattegrondFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlattegrondFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.plattegrond_main, container, false);



//        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//        textView.setText(Integer.toString(getArguments().getInt(ARG_SECTION_NUMBER)));
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        TextView wn4022 = (TextView) MainActivity.context.findViewById(R.id.wn022);
        ImageView iwn4022 = (ImageView) MainActivity.context.findViewById(R.id.iwn022);
        TextView wn4023 = (TextView) MainActivity.context.findViewById(R.id.wn023);
        ImageView iwn4023 = (ImageView) MainActivity.context.findViewById(R.id.iwn023);
        TextView wn4016 = (TextView) MainActivity.context.findViewById(R.id.wn016);
        ImageView iwn4016 = (ImageView) MainActivity.context.findViewById(R.id.iwn016);
        TextView wn4017 = (TextView) MainActivity.context.findViewById(R.id.wn017);
        ImageView iwn4017 = (ImageView) MainActivity.context.findViewById(R.id.iwn017);
        TextView wn4014 = (TextView) MainActivity.context.findViewById(R.id.wn014);
        ImageView iwn4014 = (ImageView) MainActivity.context.findViewById(R.id.iwn014);
        TextView wn4007 = (TextView) MainActivity.context.findViewById(R.id.wn007);
        ImageView iwn4007 = (ImageView) MainActivity.context.findViewById(R.id.iwn007);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String date = dateFormat.format(cal.getTime());


        for(Docent d : Docent.data){

            if(d.datum.equals(date) && !d.locatie.equals("Niet in de hogeschool")) {

                if (d.locatie.equals("4.022")) {

                    if (iwn4022.getVisibility() == View.INVISIBLE) {

                        iwn4022.setVisibility(View.VISIBLE);
                        wn4022.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4022.getText().toString())) {

                            int i = Integer.parseInt(wn4022.getText().toString());

                            i = i + 1;

                            wn4022.setText(Integer.toString(i));

                        } else {
                            wn4022.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4022.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4022.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.023")) {

                    if (iwn4023.getVisibility() == View.INVISIBLE) {

                        iwn4023.setVisibility(View.VISIBLE);
                        wn4023.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4023.getText().toString())) {

                            int i = Integer.parseInt(wn4023.getText().toString());

                            i = i + 1;

                            wn4023.setText(Integer.toString(i));

                        } else {
                            wn4023.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4023.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4023.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.016")) {

                    if (iwn4016.getVisibility() == View.INVISIBLE) {

                        iwn4016.setVisibility(View.VISIBLE);
                        wn4016.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4016.getText().toString())) {

                            int i = Integer.parseInt(wn4016.getText().toString());

                            i = i + 1;

                            wn4023.setText(Integer.toString(i));

                        } else {
                            wn4016.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4016.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4016.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.017")) {

                    if (iwn4017.getVisibility() == View.INVISIBLE) {

                        iwn4017.setVisibility(View.VISIBLE);
                        wn4017.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4017.getText().toString())) {

                            int i = Integer.parseInt(wn4017.getText().toString());

                            i = i + 1;

                            wn4017.setText(Integer.toString(i));

                        } else {
                            wn4017.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4017.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4017.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.014")) {

                    if (iwn4014.getVisibility() == View.INVISIBLE) {

                        iwn4014.setVisibility(View.VISIBLE);
                        wn4014.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4014.getText().toString())) {

                            int i = Integer.parseInt(wn4014.getText().toString());

                            i = i + 1;

                            wn4014.setText(Integer.toString(i));

                        } else {
                            wn4014.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4014.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4014.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.007")) {

                    if (iwn4007.getVisibility() == View.INVISIBLE) {

                        iwn4007.setVisibility(View.VISIBLE);
                        wn4007.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4007.getText().toString())) {

                            int i = Integer.parseInt(wn4007.getText().toString());

                            i = i + 1;

                            wn4007.setText(Integer.toString(i));

                        } else {
                            wn4007.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4007.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4007.setLayoutParams(params);
                        }

                    }

                }

            }

        }



        ImageView img022 = (ImageView) MainActivity.context.findViewById(R.id.iwn022);
        img022.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.022");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.022")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img23 = (ImageView) MainActivity.context.findViewById(R.id.iwn023);
        img23.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.023");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.023")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img16 = (ImageView) MainActivity.context.findViewById(R.id.iwn016);
        img16.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.016");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.016")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img17 = (ImageView) MainActivity.context.findViewById(R.id.iwn017);
        img17.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.017");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.017")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img14 = (ImageView) MainActivity.context.findViewById(R.id.iwn014);
        img14.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.014");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.014")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img07 = (ImageView) MainActivity.context.findViewById(R.id.iwn007);
        img07.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.007");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.007")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });




    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

    public void refresh() {

        TextView wn4022 = (TextView) MainActivity.context.findViewById(R.id.wn022);
        ImageView iwn4022 = (ImageView) MainActivity.context.findViewById(R.id.iwn022);
        wn4022.setText(null);
        iwn4022.setVisibility(View.INVISIBLE);

        TextView wn4023 = (TextView) MainActivity.context.findViewById(R.id.wn023);
        ImageView iwn4023 = (ImageView) MainActivity.context.findViewById(R.id.iwn023);
        wn4023.setText(null);
        iwn4023.setVisibility(View.INVISIBLE);

        TextView wn4016 = (TextView) MainActivity.context.findViewById(R.id.wn016);
        ImageView iwn4016 = (ImageView) MainActivity.context.findViewById(R.id.iwn016);
        wn4016.setText(null);
        iwn4016.setVisibility(View.INVISIBLE);

        TextView wn4017 = (TextView) MainActivity.context.findViewById(R.id.wn017);
        ImageView iwn4017 = (ImageView) MainActivity.context.findViewById(R.id.iwn017);
        wn4017.setText(null);
        iwn4017.setVisibility(View.INVISIBLE);

        TextView wn4014 = (TextView) MainActivity.context.findViewById(R.id.wn014);
        ImageView iwn4014 = (ImageView) MainActivity.context.findViewById(R.id.iwn014);
        wn4014.setText(null);
        iwn4014.setVisibility(View.INVISIBLE);

        TextView wn4007 = (TextView) MainActivity.context.findViewById(R.id.wn007);
        ImageView iwn4007 = (ImageView) MainActivity.context.findViewById(R.id.iwn007);
        wn4007.setText(null);
        iwn4007.setVisibility(View.INVISIBLE);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String date = dateFormat.format(cal.getTime());


        for(Docent d : Docent.data){

            if(d.datum.equals(date) && !d.locatie.equals("Niet in de hogeschool")) {

                if (d.locatie.equals("4.022")) {

                    if (iwn4022.getVisibility() == View.INVISIBLE) {

                        iwn4022.setVisibility(View.VISIBLE);
                        wn4022.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4022.getText().toString())) {

                            int i = Integer.parseInt(wn4022.getText().toString());

                            i = i + 1;

                            wn4022.setText(Integer.toString(i));

                        } else {
                            wn4022.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4022.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4022.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.023")) {

                    if (iwn4023.getVisibility() == View.INVISIBLE) {

                        iwn4023.setVisibility(View.VISIBLE);
                        wn4023.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4023.getText().toString())) {

                            int i = Integer.parseInt(wn4023.getText().toString());

                            i = i + 1;

                            wn4023.setText(Integer.toString(i));

                        } else {
                            wn4023.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4023.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4023.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.016")) {

                    if (iwn4016.getVisibility() == View.INVISIBLE) {

                        iwn4016.setVisibility(View.VISIBLE);
                        wn4016.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4016.getText().toString())) {

                            int i = Integer.parseInt(wn4016.getText().toString());

                            i = i + 1;

                            wn4023.setText(Integer.toString(i));

                        } else {
                            wn4016.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4016.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4016.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.017")) {

                    if (iwn4017.getVisibility() == View.INVISIBLE) {

                        iwn4017.setVisibility(View.VISIBLE);
                        wn4017.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4017.getText().toString())) {

                            int i = Integer.parseInt(wn4017.getText().toString());

                            i = i + 1;

                            wn4017.setText(Integer.toString(i));

                        } else {
                            wn4017.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4017.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4017.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.014")) {

                    if (iwn4014.getVisibility() == View.INVISIBLE) {

                        iwn4014.setVisibility(View.VISIBLE);
                        wn4014.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4014.getText().toString())) {

                            int i = Integer.parseInt(wn4014.getText().toString());

                            i = i + 1;

                            wn4014.setText(Integer.toString(i));

                        } else {
                            wn4014.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4014.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4014.setLayoutParams(params);
                        }

                    }

                }

                if (d.locatie.equals("4.007")) {

                    if (iwn4007.getVisibility() == View.INVISIBLE) {

                        iwn4007.setVisibility(View.VISIBLE);
                        wn4007.setText(d.docentCode);

                    } else {

                        if (isNumeric(wn4007.getText().toString())) {

                            int i = Integer.parseInt(wn4007.getText().toString());

                            i = i + 1;

                            wn4007.setText(Integer.toString(i));

                        } else {
                            wn4007.setText("2");
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) wn4007.getLayoutParams();
                            params.setMargins(0, 0, 65, 55);
                            wn4007.setLayoutParams(params);
                        }

                    }

                }

            }

        }



        ImageView img022 = (ImageView) MainActivity.context.findViewById(R.id.iwn022);
        img022.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.022");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.022")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img23 = (ImageView) MainActivity.context.findViewById(R.id.iwn023);
        img23.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.023");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.023")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img16 = (ImageView) MainActivity.context.findViewById(R.id.iwn016);
        img16.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.016");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.016")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img17 = (ImageView) MainActivity.context.findViewById(R.id.iwn017);
        img17.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.017");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.017")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img14 = (ImageView) MainActivity.context.findViewById(R.id.iwn014);
        img14.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.014");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.014")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

        ImageView img07 = (ImageView) MainActivity.context.findViewById(R.id.iwn007);
        img07.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
                alertDialog.setTitle("WN.4.007");
                String message = "";

                for(Docent d : Docent.data) {

                    if (d.locatie.equals("4.007")) {

                        if (message.equals("")) {
                            message = d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }
                        else {
                            message = message + "\n" + d.docentNaam + " | Laatst gezien: " + d.tijd;
                        }

                    }

                }

                alertDialog.setMessage(message);
                alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                alertDialog.show();
            }
        });

    }
}