package hr.app.docentfinder.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;


/**
 * Created by Michael Parry on 22-4-2014.
 */
public class SplashScreen extends Activity{
    // Splash screen timer


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        processJson();
    }
    public void processJson(){

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://docentfinder.martijnschipper.com/get_json.php", new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String response) {
                Docent.data = new ArrayList<Docent>();
                JSONObject obj;
                try {
                    obj = new JSONObject(response);
                    JSONArray locations = obj.getJSONArray("locations");

                    for (int i = 0; i < locations.length(); i++) {
                        JSONObject l = locations.getJSONObject(i);

                        String id           = l.getString(MainActivity.TAG_ID);
                        String docentNaam   = l.getString(MainActivity.TAG_DOCENTNAAM);
                        String docentCode   = l.getString(MainActivity.TAG_DOCENTCODE);
                        String locatie      = l.getString(MainActivity.TAG_LOCATIE);
                        String datum        = l.getString(MainActivity.TAG_DATUM);
                        String tijd         = l.getString(MainActivity.TAG_TIJD);
                        String verschil     = l.getString(MainActivity.TAG_VERSCHIL);

                        Docent docent = new Docent(id, docentCode, docentNaam, locatie, datum, tijd, verschil);
                        Docent.data.add(docent);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Intent i = new Intent(SplashScreen.this, MainActivity.class);
                startActivity(i);
                finish();
            }
        });
    }
}
