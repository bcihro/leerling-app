package hr.app.docentfinder.app;

/**
 * Created by Michael Parry on 17-4-2014.
 */

import android.app.AlertDialog;
import android.app.ListFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import com.loopj.android.http.*;
import org.json.*;
import java.util.Comparator;
import java.util.ArrayList;


/**
 * A placeholder fragment containing a simple view.
 */
public class LijstFragment extends ListFragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER  = "section_number";

    private ArrayAdapter<String> adapter;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static LijstFragment newInstance(int sectionNumber) {
        LijstFragment fragment = new LijstFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public LijstFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.lijst_main, container, false);
        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<String> docentData = new ArrayList<String>();
        for(Docent d : Docent.data){
            docentData.add(d.docentNaam + " (" + d.docentCode + ")");
        }

        adapter = new DocentAdapter(getActivity(), R.layout.list_block, docentData);

        adapter.sort(new Comparator<String>() {
            public int compare(String object1, String object2) {
                return object2.compareToIgnoreCase(object1);
            };
        });

        setListAdapter(adapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        Docent docentData = Docent.data.get(position);

        AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.context).create();
        alertDialog.setTitle(docentData.docentNaam + " (" + docentData.docentCode + ")");
        alertDialog.setMessage("Lokaal: " + docentData.locatie + "\nLaatst gezien: " + docentData.tijd);
        alertDialog.setButton("Sluiten", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // TODO Add your code for the button here.
            }
        });
        // Set the Icon for the Dialog
        //alertDialog.setIcon(R.drawable.icon);
        alertDialog.show();
    }

    public void Refresh(){

        ArrayList<String> docentData = new ArrayList<String>();
        for(Docent d : Docent.data){
            docentData.add(d.docentNaam + " (" + d.docentCode + ")");
        }

        adapter.clear();
        adapter.addAll(docentData);

        adapter.notifyDataSetChanged();



    }



}