package hr.app.docentfinder.app;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Michael Parry on 24-4-2014.
 */
public class InfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }
}
